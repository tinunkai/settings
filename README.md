# Settings
Repository of my settings of vim, zsh, getmail, mutt and openbox.

The mutt settings are based on [annvix](https://annvix.com/using_mutt_on_os_x).
